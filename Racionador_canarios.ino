

            /*********
 *programa escrito para hacer funcionar el carro de comida automatica del modulo
  de cria de canarios 16/5/2021
  por Miguel ANgel Gomez
  Nodemcu pinout: https://i2.wp.com/www.esploradores.com/wp-content/uploads/2016/08/PINOUT-NodeMCU_1.0-V2-y-V3.png?fit=2323%2C1590
  rtc3231 clock 8266 d1
  rtc3231 data 8266 d2
*********/

// Import required libraries
#include <ESP8266WiFi.h>
#include <ESPAsyncTCP.h>
#include <ESPAsyncWebServer.h>
#include <Servo.h>
#include <WiFiUdp.h>
#include <Time.h>
#include <Timezone_Generic.h>
#include <TimeAlarms.h>
#include <EEPROM.h>

//defines para mejorar la legibilidad del codigo
#define REPOSO 0
#define AVANCE 1
#define RETROCESO 2

//ntpclient
unsigned int localPort = 2390;      // local port to listen for UDP packets
IPAddress timeServerIP; // time.nist.gov NTP server address
const char* ntpServerName = "time.nist.gov";
const int NTP_PACKET_SIZE = 48; // NTP time stamp is in the first 48 bytes of the message
byte packetBuffer[ NTP_PACKET_SIZE]; //buffer to hold incoming and outgoing packets
const int tzAdjust=3600*-3;
// A UDP instance to let us send and receive packets over UDP
WiFiUDP udp;


// Replace with your network credentials
const char* ssid = "Airedata_7602_WiFi";
const char* password = "83172182";


const long int tz=-10800; //segundos de la zona horaria local(utc-3)

//variables para programas de temporizacion
unsigned char cantProgs=0;
unsigned char lastProg_n=0;
time_t lastProg_t;


//variables de estados
bool varFinal1=0;
bool varFinal2=0;
bool varFinal1Prev=0;
bool varFinal2Prev=0;
bool pausarMotor=0;
unsigned char estadoMotor=0;
bool timePulse=0;
bool sensorPast=0;

//para raciones
int racionesLimit=0;
int racionesDadas=0;


/*pines usados */
//usar d3 (0) y d4 (2)  para el driver del motor
const int motor1=0;
const int motor2=2;
// usar d1 (5) para el sensor hall
const int sensorHall=5;
//usar d2 (4) para el servo
const int final1=12;
const  int final2=14;
 int contador_actualizacion=0;
//usar d2 (4) para el servo
const int servoPin=4;
Servo myservo;  // create servo object to control a servo


// Create AsyncWebServer object on port 80
AsyncWebServer server(80);
AsyncWebSocket ws("/ws");


////FIN VARIABLES//////////////


/************************************************************
 * *******************************************************
      ***************** PAGINA WEB********************
 * *******************************************************
*************************************************************/
const char index_html[] PROGMEM = R"rawliteral(
<!DOCTYPE HTML><html>
<head>
  <title>ESP Web Server</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="icon" href="data:,">
  <style>
  html {
    font-family: Arial, Helvetica, sans-serif;
    text-align: center;
  }
  h1 {
    font-size: 1.8rem;
    color: white;
  }
  h2{
    font-size: 1.5rem;
    font-weight: bold;
    color: #143642;
  }
  .topnav {
    overflow: hidden;
    background-color: #143642;
  }
  body {
    margin: 0;
  }
  .content {
    padding: 30px;
    max-width: 600px;
    margin: 0 auto;
  }
  .card {
    background-color: #F8F7F9;;
    box-shadow: 2px 2px 12px 1px rgba(140,140,140,.5);
    padding-top:10px;
    padding-bottom:20px;
  }
  .button {
    padding: 15px 50px;
    font-size: 24px;
    text-align: center;
    outline: none;
    color: #fff;
    background-color: #0f8b8d;
    border: none;
    border-radius: 5px;
    -webkit-touch-callout: none;
    -webkit-user-select: none;
    -khtml-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    -webkit-tap-highlight-color: rgba(0,0,0,0);
   }
   /*.button:hover {background-color: #0f8b8d}*/
   .button:active {
     background-color: #0f8b8d;
     box-shadow: 2 2px #CDCDCD;
     transform: translateY(2px);
   }
   .state {
     font-size: 1.5rem;
     color:#8c8c8c;
     font-weight: bold;
   }
  </style>
<title>ESP Web Server</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="icon" href="data:,">
</head>
<body>
  <div class="topnav">
    <h1>ESP WebSocket Server</h1>
  </div>
  <div class="content">
    <div class="card">
      <h2>Racionador balanceado</h2>
      <p>
      <button id="final1" class="button">Avanzar</button>
      <button id="final2" class="button">Retroceder</button>
      </p>
<!--      <p class="state">Estado: <span id="state">%FINAL1%</span></p>
      <p><button id="final1" class="button">Avanzar</button></p>

      <p class="state">Estado: <span id="state">%FINAL2%</span></p>
      <p><button id="final2" class="button">Retroceder</button></p>
    --> 
    </div>
    <div>
      <p > Ultima actualizacion de hora: <span id="tiempo">%TIEMPO%</span></p>
    </div>
  </div>
<script>
  var gateway = `ws://${window.location.hostname}/ws`;
  var websocket;
  window.addEventListener('load', onLoad);
  function initWebSocket() {
    console.log('Trying to open a WebSocket connection...');
    websocket = new WebSocket(gateway);
    websocket.onopen    = onOpen;
    websocket.onclose   = onClose;
    websocket.onmessage = onMessage; // <-- add this line
  }
  function onOpen(event) {
    console.log('Connection opened');
  }
  function onClose(event) {
    console.log('Connection closed');
    setTimeout(initWebSocket, 2000);
  }
  function onMessage(event) {
    //var state;
    console.log(event.data);
    if (event.data == "1"){
      //state = "ON";
         // document.getElementById('state').innerHTML = state;
document.getElementById('state').innerHTML ="ON";
    }
    else if (event.data == "0"){
      //state = "OFF";
        //  document.getElementById('state').innerHTML = state;
document.getElementById('state').innerHTML ="OFF";
    }
    else if (event.data =="F11"){
      document.getElementById('final1').innerHTML ="Avanzar";

    }
    else if (event.data =="F10"){
      document.getElementById('final1').innerHTML ="------";
    
    }else if (event.data =="F21"){
      document.getElementById('final2').innerHTML ="Retroceder";

    }
    else if (event.data =="F20"){
      document.getElementById('final2').innerHTML ="-----";
    
    }else{
            document.getElementById('tiempo').innerHTML =event.data;

    }


  }
  function onLoad(event) {
    initWebSocket();
    initButton();
  }
  function initButton() {
    document.getElementById('final1').addEventListener('click', final1);
    document.getElementById('final2').addEventListener('click', final2);

  }
  function final1(){
    websocket.send('final1');
  }
  function final2(){
    websocket.send('final2');
  }
</script>
</body>
</html>
)rawliteral";


/********************************
  ******* FIN PAGINA***********
*********************************/


void motor( unsigned char direccion){
    digitalWrite(motor1,LOW);
    digitalWrite(motor2,LOW);
    digitalWrite(direccion,HIGH);

}

void motor(void){
  digitalWrite(motor1,LOW);
    digitalWrite(motor2,LOW);
}
void notifyTime(time_t t){
  String result="";
  result+=day(t);
  result+="/";
  result+=month(t);
  result+="/";
  result+=year(t);
  result+="  ";
  result+=hour(t);
  result+=":";
  result+=minute(t);
  ws.textAll(result);
}

void notifyClients2(unsigned char final, bool varFinal) {
  if(final==1){
    if(!varFinal){
     ws.textAll(String("F11"));

    }else{
      ws.textAll(String("F10"));
    }
  }else{
    if(!varFinal){
     ws.textAll(String("F21"));

    }else{
      ws.textAll(String("F20"));
    }
    
  }

}

void handleWebSocketMessage(void *arg, uint8_t *data, size_t len) {
  AwsFrameInfo *info = (AwsFrameInfo*)arg;
  if (info->final && info->index == 0 && info->len == len && info->opcode == WS_TEXT) {
    data[len] = 0;
    if (strcmp((char*)data, "final1") == 0) {
      Serial.println("boton1");
      if (estadoMotor==REPOSO || estadoMotor==RETROCESO){
        if(varFinal2){
          estadoMotor=AVANCE;
        }
      }else{
        estadoMotor=REPOSO;
      }
    }else if (strcmp((char*)data, "final2") == 0) {
      Serial.println("boton2");
      if (estadoMotor==REPOSO || estadoMotor==AVANCE){
        if(varFinal1){
          estadoMotor=RETROCESO;
        }
      }else{
        estadoMotor=REPOSO;
      }
    }
  }
}

void onEvent(AsyncWebSocket *server, AsyncWebSocketClient *client, AwsEventType type,
             void *arg, uint8_t *data, size_t len) {
    switch (type) {
      case WS_EVT_CONNECT:
        Serial.printf("WebSocket client #%u connected from %s\n", client->id(), client->remoteIP().toString().c_str());
        break;
      case WS_EVT_DISCONNECT:
        Serial.printf("WebSocket client #%u disconnected\n", client->id());
        break;
      case WS_EVT_DATA:
        handleWebSocketMessage(arg, data, len);
        break;
      case WS_EVT_PONG:
      case WS_EVT_ERROR:
        break;
  }
}

void initWebSocket() {
  ws.onEvent(onEvent);
  server.addHandler(&ws);
}

String processor(const String& var){
  Serial.println(var);
   if(var == "FINAL1"){
    if( !varFinal1 ){
      return "Activo";
    }else{
      return "Libre";
    }
  }else if(var == "FINAL2"){
    if( !varFinal2 ){
      return "Activo";
    }else{
      return "Libre";
    }
  }
 }

//*funciones de ntp client*//
void sendNTPpacket(IPAddress& address) {
  Serial.println("sending NTP packet...");
  // set all bytes in the buffer to 0
  memset(packetBuffer, 0, NTP_PACKET_SIZE);
  // Initialize values needed to form NTP request
  // (see URL above for details on the packets)
  packetBuffer[0] = 0b11100011;   // LI, Version, Mode
  packetBuffer[1] = 0;     // Stratum, or type of clock
  packetBuffer[2] = 6;     // Polling Interval
  packetBuffer[3] = 0xEC;  // Peer Clock Precision
  // 8 bytes of zero for Root Delay & Root Dispersion
  packetBuffer[12]  = 49;
  packetBuffer[13]  = 0x4E;
  packetBuffer[14]  = 49;
  packetBuffer[15]  = 52;

  // all NTP fields have been given values, now
  // you can send a packet requesting a timestamp:
  udp.beginPacket(address, 123); //NTP requests are to port 123
  udp.write(packetBuffer, NTP_PACKET_SIZE);
  udp.endPacket();
}

////////imprime fecha desde datetime
void printDateTime(time_t t, const char *tz)
{
  char buf[32];
  char m[4];    // temporary storage for month string (DateStrings.cpp uses shared buffer)
  strcpy(m, monthShortStr(month(t)));
  sprintf(buf, "%.2d:%.2d:%.2d %s %.2d %s %d %s",
          hour(t), minute(t), second(t), dayShortStr(weekday(t)), day(t), m, year(t), tz);
  Serial.println(buf);
}





void actualizarHora(void){

  WiFi.hostByName(ntpServerName, timeServerIP);

  sendNTPpacket(timeServerIP); // send an NTP packet to a time server
  // wait to see if a reply is available
  delay(1000);
  
  int cb = udp.parsePacket();
  unsigned char cont=0;
  while (!cb) {
    cont++;
    Serial.println("no packet yet");
    delay(1000);
    if (cont==10) break;
  } 
  if(cb){
    Serial.println("Actualizando hora");
    Serial.print("packet received, length=");
    Serial.println(cb);
    // We've received a packet, read the data from it
    udp.read(packetBuffer, NTP_PACKET_SIZE); // read the packet into the buffer

    //the timestamp starts at byte 40 of the received packet and is four bytes,
    // or two words, long. First, esxtract the two words:

    unsigned long highWord = word(packetBuffer[40], packetBuffer[41]);
    unsigned long lowWord = word(packetBuffer[42], packetBuffer[43]);
    // combine the four bytes (two words) into a long integer
    // this is NTP time (seconds since Jan 1 1900):
    unsigned long secsSince1900 = highWord << 16 | lowWord;
    Serial.print("Seconds since Jan 1 1900 = ");
    Serial.println(secsSince1900);

    // now convert NTP time into everyday time:
    Serial.print("Unix time = ");
    // Unix time starts on Jan 1 1970. In seconds, that's 2208988800:
    const unsigned long seventyYears = 2208988800UL;
    // subtract seventy years:
    unsigned long epoch = secsSince1900 - seventyYears;
    // print Unix time:
    Serial.println(epoch);
    ////Actualizando el rtc
    //rtc.now( DateTime((uint32_t) epoch) );
    time_t n=epoch;
    
    // print the hour, minute and second:
    Serial.print("The UTC time is ");       // UTC is the time at Greenwich Meridian (GMT)
    Serial.print((epoch  % 86400L) / 3600); // print the hour (86400 equals secs per day)
    Serial.print(':');
    if (((epoch % 3600) / 60) < 10) {
      // In the first 10 minutes of each hour, we'll want a leading '0'
      Serial.print('0');
    }
    Serial.print((epoch  % 3600) / 60); // print the minute (3600 equals secs per minute)
    Serial.print(':');
    if ((epoch % 60) < 10) {
      // In the first 10 seconds of each minute, we'll want a leading '0'
      Serial.print('0');
    }
    Serial.println(epoch % 60); // print the second

    Serial.println("Tiempo Local Actualizado");
    n+=tzAdjust;
    setTime(n);
    serialTime(now());
  }else {
    Serial.println("No se pudo actualizar la hora");
  }
}

void serialTime(time_t time_n){
  notifyTime(time_n);
  Serial.println("Fecha:");
  Serial.print(day(time_n));
  Serial.print("/");
  Serial.print(month(time_n));
  Serial.print("/");
  Serial.println(year(time_n));
  Serial.println("HORA:");
  Serial.print(hour(time_n));
  Serial.print(":");
  Serial.print(minute(time_n));
  Serial.print(":");
  Serial.println(second(time_n));
}

void serialTime(void){
  time_t time_n=now();
  notifyTime(time_n);
  Serial.println("Fecha:");
  Serial.print(day(time_n));
  Serial.print("/");
  Serial.print(month(time_n));
  Serial.print("/");
  Serial.println(year(time_n));
  Serial.println("HORA:");
  Serial.print(hour(time_n));
  Serial.print(":");
  Serial.print(minute(time_n));
  Serial.print(":");
  Serial.println(second(time_n));
}


void descargaServo(void){
    myservo.write(0);
    Alarm.timerOnce(2,vueltaServo); 
}

void vueltaServo(void){
  Serial.println("Volviendo servo");
  myservo.write(90);
  racionesDadas++;
  if (racionesDadas<racionesLimit){
    Alarm.timerOnce(2,descargaServo);
  }else{
    pausarMotor=0;
  }
}


//funcion que cuenta los programas guardados en eeprom
unsigned char contar_programas(void){
  unsigned int addr=0;
  unsigned char result=0;
  unsigned char last_t= EEPROM.read(addr);
  while(last_t != 0){
    result++;
    addr+=4;
    last_t= EEPROM.read(addr);
    
  }
  return result;
}

//funcion que devuelve el tiempo almacenado en el lugar de eeprom
time_t tiempo_eeprom(unsigned int pos){
  time_t result=0;
  for (int n=0; n<4; n++){
    result=result*(8*n) + EEPROM.read(pos+n);
  }
  return result;
}


void setup(){
  // Serial port for debugging purposes
  Serial.begin(115200);
  EEPROM.begin(512);
    
  pinMode(motor1, OUTPUT);
  pinMode(motor2, OUTPUT);
  pinMode(final1, INPUT_PULLUP);
  pinMode(final2, INPUT_PULLUP);
  pinMode(sensorHall,INPUT_PULLUP);
  pinMode(servoPin,OUTPUT);

  cantProgs=contar_programas();
  Serial.println("Cantidad de Programas guardados");
  Serial.println(cantProgs);
  for (int n=0; n<cantProgs;n++){
    serialTime(tiempo_eeprom(n));
  }
  
  // Connect to Wi-Fi
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(1000);
    Serial.println("Connecting to WiFi..");
  }

  // Print ESP Local IP Address
  Serial.println(WiFi.localIP());

  initWebSocket();

  // Route for root / web page
  server.on("/", HTTP_GET, [](AsyncWebServerRequest *request){
    request->send_P(200, "text/html", index_html, processor);
  });

  // Start server
  server.begin();
  udp.begin(localPort);

  myservo.attach(servoPin);
  myservo.write(90);
    
  Serial.println("Actualizando hora ...");
  actualizarHora();
    
  Alarm.alarmRepeat(8,0,0,actualizarHora);
  Alarm.alarmRepeat(16,0,0,actualizarHora);
  Alarm.timerRepeat(60,serialTime);

  Serial.println("Hora del sistema");
  time_t t=now();
  Serial.println("Fecha:");
   Serial.print(day(t));
   Serial.print("/");
   Serial.print(month(t));
   Serial.print("/");
   Serial.println(year(t));
   Serial.println("HORA:");
   Serial.print(hour(t));
   Serial.print(":");
   Serial.print(minute(t));
   Serial.print(":");
   Serial.println(second(t));
  Serial.println("FIN SETUP");
  
}

void loop() {
  
  Alarm.delay(0);
  ws.cleanupClients();
  
  varFinal1=digitalRead(final1);
  if (varFinal1 != varFinal1Prev){
    notifyClients2(1,varFinal1);
    varFinal1Prev=varFinal1;
  }
  varFinal2=digitalRead(final2);
  if (varFinal2 != varFinal2Prev){
    notifyClients2(2,varFinal2);
    varFinal2Prev=varFinal2;
  }
  
switch (estadoMotor){
  case REPOSO://motor detenido
    if (timePulse){//variabe bool que se activa por alarma del rtc en los horarios preseleccionados
      if(!varFinal1){
        estadoMotor=AVANCE;
        
      }else if (!varFinal2){
        estadoMotor=RETROCESO;
      }
      timePulse=0;//resetea el flag de timer
    }
    break;
  case AVANCE://motor avanzando
    if(!varFinal2){//final de carrera trasero
      estadoMotor=REPOSO;
    }
    break;
  case RETROCESO://motor volviendo
    if(!varFinal1){//final de carrera delantero
      estadoMotor=REPOSO;
    }
    break;
}

bool sensorNow=digitalRead(sensorHall);
if(sensorNow!=sensorPast && estadoMotor!=REPOSO){
  sensorPast=sensorNow;
  if(!sensorPast) {
    pausarMotor=1;
    racionesLimit=2;
    racionesDadas=0;
    descargaServo();
  }
}

  /*
   * actualizacion de las salidas   
  */

  if (estadoMotor==REPOSO || pausarMotor){
    motor();
  }else if(estadoMotor==AVANCE){
    motor(motor1);
  }else if(estadoMotor==RETROCESO){
    motor(motor2);
  }


}

